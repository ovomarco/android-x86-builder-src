FROM notovoauto/android-x86-builder-deps


#----------------------- install repo tool ------------------------------------
ENV XBIN=/usr/local/bin
RUN wget -O ${XBIN}/repo https://storage.googleapis.com/git-repo-downloads/repo
RUN chmod a+x ${XBIN}/repo


ARG CROOT=/croot
RUN mkdir $CROOT && cd $CROOT

WORKDIR $CROOT

RUN chown ${USERNAME}:${USERNAME} $CROOT
ARG MANIFEST_URL=git://git.osdn.net/gitroot/android-x86/manifest
ARG MANIFEST_BRANCH=oreo-x86
ARG MANIFEST_NAME=default.xml



RUN mkdir -p ~/.ssh
RUN ssh-keyscan git.osdn.net >> ~/.ssh/known_hosts

RUN git config --global user.name "Your Name"
RUN git config --global user.email "you@example.com"

RUN set -x && repo init \
  --manifest-url="$MANIFEST_URL" \
  --manifest-name="$MANIFEST_NAME" \
  --manifest-branch="$MANIFEST_BRANCH" \
  --no-clone-bundle\
  --no-repo-verify\
  --depth=1
RUN repo sync \
  --current-branch \
  --no-clone-bundle \
  --jobs=$(nproc --all)

#TODO also add a local manifest for gapps and do git lfs


